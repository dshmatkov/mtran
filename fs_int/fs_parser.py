import copy
from ply import yacc
from fs_lexer import FSLexer


class SyntaxNode(object):

    def __init__(self, type, value, children):
        self.children = children
        self.type = type
        self.value = value

    def __str__(self):
        if self.value is not None:
            return "{}: {}".format(self.type, self.value)
        else:
            return "{}: ".format(self.type)

    def __repr__(self):
        if self.value is not None:
            return "{}: {}".format(self.type, self.value)
        else:
            return "{}: ".format(self.type)

    def prettify(self, indent=0):
        tree = " " * indent + str(self) + "\n"
        for child in self.children:
            if isinstance(child, SyntaxNode):
                tree += child.prettify(indent + 6)
            else:
                tree += " " * (indent + 6) + str(child) + '\n'
        return tree


class FSParser(FSLexer):

    fs_functions = {
        "System": {"type": "module", "parameters": []},
        "Console": {"type": "module", "parameters": []},
        "ReadLine": {"type": "function", "parameters": []},
        "WriteLine": {"type": "function", "parameters": ["string"]},
        "Split": {"type": "function", "parameters": ["string"]},
        "Lenght": {"type": "function", "parameters": []},
    }

    def __init__(self):
        super(FSParser, self).__init__()
        self.syntax_errors = []
        self.semantic_errors = []
        self.identifiers = self.fs_functions.copy()

    def p_error(self, p):
        self.syntax_errors.append(p)

    def p_statements_block(self, p):
        """
        statements_block : statements_block new_line statement
                         | statement
        """
        if len(p) == 2:
            p[0] = SyntaxNode("statements_block", None, [p[1]])
        else:
            p[1].children.append(p[3])
            p[0] = p[1]
        # print p.slice

    def p_statement(self, p):
        """
        statement : loop_with_parameter
                  | variable_declaration
                  | condition
                  | assignment
                  | expression
                  | module_import
        """
        p[0] = p[1]
        # print p.slice

    def p_module_import(self, p):
        """
        module_import : open identifier
        """
        p[2] = SyntaxNode('name', p[2], [])
        p[0] = SyntaxNode('module_import', None, [p[2]])

    def p_condition(self, p):
        """
        condition : if expression then new_line statements_block else new_line statements_block
                  | if expression then new_line statements_block
        """
        if len(p) == 9:
            p[1] = SyntaxNode('condition', None, [p[2], p[5], p[8]])
            p[0] = p[1]
        else:
            p[1] = SyntaxNode('condition', None, [p[2], p[5]])
            p[0] = p[1]
        # print p.slice

    def p_variable_declaration(self, p):
        """
        variable_declaration : let identifier assign expression
        """
        if p[2] not in self.identifiers:
            self.identifiers[p[2]] = {"type": "variable"}
        p[2] = SyntaxNode("name", p[2], [])
        p[0] = SyntaxNode("variable_declaration", None, [p[2], p[4]])
        # print p.slice

    def p_expression(self, p):
        """
        expression : open_parenthesis expression close_parenthesis
                   | expression operator expression
                   | expression operator operand
                   | operand
        """
        if len(p) == 2:
            p[0] = p[1]
        elif len(p) == 4 and p[1] == "(" and p[3] == ")":
            p[0] = p[2]
        else:
            p[0] = SyntaxNode("expression", None, [p[1], p[2], p[3]])
        # print p.slice

    def p_assignment(self, p):
        """
        assignment : identifier insert expression
                   | array_cell insert expression
        """
        if p.slice[1].type == "identifier" and p[1] not in self.identifiers:
            error_format = "Error in line {}: Identifier '{}' is not declared"
            self.semantic_errors.append(error_format.format(p.slice[1].lineno, p[1]))

        if p.slice[1].type == 'identifier':
            p[1] = SyntaxNode("variable", p[1], [])
        p[0] = SyntaxNode("assignment", None, [p[1], p[3]])
        # print p.slice

    def p_method_call(self, p):
        """
        method_call : method_call dot function_call
                    | method_call dot identifier
                    | identifier dot function_call
                    | identifier dot identifier
        """
        if p.slice[1].type == 'identifier':
            p[0] = SyntaxNode("method_call", None, [p[1], p[3]])
        else:
            p[1].children.append(p[3])
            p[0] = p[1]

        # print p.slice

    def p_loop_with_parameter(self, p):
        """
        loop_variable_declaration : identifier

        loop_with_parameter : for loop_variable_declaration in expression dotdot expression do new_line statements_block
        """
        if len(p) == 2:
            if p[1] not in self.identifiers:
                self.identifiers[p[1]] = {"type": "variable"}

            p[0] = SyntaxNode('variable', p[1], [])
        else:
            temp = SyntaxNode('diapason', None, [p[4], p[6]])
            p[1] = SyntaxNode("for", None, [p[2], temp])
            p[0] = SyntaxNode("loop_with_parameter", None, [p[1], p[9]])
        # print p.slice

    def p_function_call(self, p):
        """
        function_call : identifier open_parenthesis parameters close_parenthesis
        """
        p[0] = SyntaxNode("function_call", None, [p[1], p[3]])
        if p[1] not in self.identifiers:
            error_format = "Error in line {}: Function '{}' is not declared"
            self.semantic_errors.append(error_format.format(p.slice[1].lineno, p[1]))
        elif self.identifiers[p[1]]['type'] != 'function':
            error_format = "Error in line {}: '{}' is not a function"
            self.semantic_errors.append(error_format.format(p.slice[1].lineno, p[1]))
        elif len(p[3].children) != len(self.identifiers[p[1]]['parameters']):
            error_format = "Error in line {}: Wrong parameters count in function '{}'"
            self.semantic_errors.append(error_format.format(p.slice[1].lineno, p[1]))

        # print p.slice

    def p_function_parameters(self, p):
        """
        parameters : parameters comma expression
                   | expression
                   |
        """
        if len(p) == 2:
            p[0] = SyntaxNode('parameters', None, [p[1]])
        elif len(p) == 4:
            p[1].children.append(p[2])
            p[0] = p[1]
        else:
            p[0] = SyntaxNode('parameters', None, [])
        # print p.slice

    def p_operator(self, p):
        """
        operator : multiply
                 | divide
                 | subtract
                 | add
                 | greater
                 | lower
                 | greater_or_equal
                 | lower_or_equal
                 | logical_or
                 | logical_and
                 | equal
                 | module
        """
        p[0] = SyntaxNode('operator', p[1], [])
        # print p.slice

    def p_array_cell(self, p):
        """
        array_cell : identifier dot open_bracket expression close_bracket
        """
        p[0] = SyntaxNode('array_cell', None, [p[1], p[4]])
        if p[1] not in self.identifiers:
            error_format = "Error in line {}: Identifier '{}' is not declared"
            self.semantic_errors.append(error_format.format(p.slice[1].lineno, p[1]))
        # print p.slice

    def p_operand(self, p):
        """
        operand : array_cell
                | method_call
                | function_call
                | float_number
                | integer_number
                | string
                | char
                | identifier
        """
        p[0] = SyntaxNode(p.slice[1].type, None, [p[1]])
        if p.slice[1].type == "identifier" and p[1] not in self.identifiers:
            error_format = "Error in line {}: Identifier '{}' is not declared"
            self.semantic_errors.append(error_format.format(p.slice[1].lineno, p[1]))

    def syntax_check(self, code):
        parser = yacc.yacc(module=self, check_recursion=False)
        syntax_tree = parser.parse(code)
        if self.syntax_errors or self.semantic_errors:
            raise RuntimeError()
        else:
            return syntax_tree.prettify()


if __name__ == '__main__':
    with open("tests/code.fs", "r") as code:
        fs_parser = FSParser()
        try:
            print fs_parser.syntax_check(code.read())
        except RuntimeError as e:
            for error in fs_parser.syntax_errors:
                print "Unexpected token '{}' in line {}".format(error.value, error.lineno)
            for error in fs_parser.semantic_errors:
                print error
